'use strict';

const express = require('express');
let helpers = require('./helpers');
helpers = new helpers();
let request = require('./request');
request = new request();

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
const app = express();

// Populate Users array
let users = [];
async function requestUsers() {
  users = await request.getData();
}
requestUsers();


// App
app.get('/', (req, res) => {
  res.send( '<h3>1. Os websites de todos os usuários</h3><br/>'
          + '<ul>'
          + helpers.retrieveAllUsersWebsites(users).map( website => '<li>' + website + '</li>')
          + '</ul>'
          + '<br/><br/>'
          + '<h3>2. O Nome, email e a empresa em que trabalha (em ordem alfabética)</h3><br/>'
          + '<ul>'
          + helpers.retrieveNameEmailAndCompany(users).map( user => '<li>' + user.name + ', ' + user.email + ', ' + user.company + '</li>')
          + '</ul>'
          + '<br/><br/>'
          + '<h3>3. Mostrar todos os usuários que no endereço contem a palavra ```suite```</h3><br/>'
          + '<ul>'
          + helpers.retrieveUsersLivingInSuites(users).map( user => '<li>' + user.name + ', ' + user.address.suite + '</li>')
          + '</ul>'
          + '<br/>');
  //res.send('Hello world\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);