'use strict';

const chai = require('chai');
const expect = chai.expect;

let helpers = require('./helpers');
helpers = new helpers();
const users = require('./users.json');

describe('Testes para o componente helpers.js:', () => {
  context('Os websites de todos os usuários.', () => {
    it('Deveria receber um array, não vazio.', () => {
      expect(helpers.retrieveAllUsersWebsites(users)).to.be.an('array').to.not.be.empty;
    });
  });

  context('O Nome, email e a empresa em que trabalha (em ordem alfabética).', () => {
    it('Deveria receber um array, não vazio.', () => {
      expect(helpers.retrieveNameEmailAndCompany(users)).to.be.an('array').to.not.be.empty;;
    });
  });

  context('Mostrar todos os usuários que no endereço contem a palavra suite.', () => {
    it('Deveria receber um array, não vazio.', () => {
      expect(helpers.retrieveUsersLivingInSuites(users)).to.be.an('array').to.not.be.empty;;
    });
  });
});