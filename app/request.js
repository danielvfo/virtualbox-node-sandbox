'use strict';

module.exports = class request {
  constructor() {};

  async getData() {
    const options = {
      hostname: 'jsonplaceholder.typicode.com',
      path: '/users',
      method: 'GET'
    };

    let users = await this.httpsRequest(options);
    return users;
  }

  httpsRequest(options) {
    return new Promise((resolve, reject) => {
      const https = require('https');
      const request = https.request(options, (response) => {
        var data = '';
        response.on('data', function (chunk) {
          data += chunk;
        });
        response.on('end', function () {
          resolve(JSON.parse(data));
        });
      });
      request.on('error', (error) => {
        reject(error);
      });
      request.end();
    });
  }
}
