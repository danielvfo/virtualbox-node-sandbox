**Projeto desenvolvido para Coding Challenge**

Este projeto foi desenvolvido para um Coding Challenge. As principais tecnologias utilizadas são: Vagrant, Docker, node.js, Unit-testing com Mocha e Chai.

---

## Iniciando o projeto

1. git clone via ssh ou https:

	  1.1. SSH: git clone git@bitbucket.org:danielvfo/virtualbox-node-sandbox.git
	  
	  1.2. HTTPS: git clone https://danielvfo@bitbucket.org/danielvfo/virtualbox-node-sandbox.git
	  
2. cd virtualbox-node-sandbox/
3. vagrant up
4. http://localhost:8080/ de seu navegador

---

## Considerações

1. O app (server) feito em node reside em /app
2. O arquivo users.json, é utlizado apenas para os testes em mocha/chai, o app server.js faz uma requisão https para buscar os usuários
3. O tópico sobre elasticsearch, conforme enunciado, não foi implementado, acho que não entendi como ele entra nesse jogo. Até porque eu não o conhecia, só havia ouvido sobre, tentei ler sobre, mas não rolou =( (desculpa gente!)
4. Já peço desculpas de antemão em relação às boas práticas de estruturação dos projeto, muita coisa era novidade durante a concepção deste projeto, aprendi muito, gostei =)

---

## Finalizando o projeto

1. Desligar a vm ou desligar e apagar seu arquivos:

	1.1. vagrant halt - para desligar a vm
	
	1.2. vagrant destroy - para desligar a vm e apagar seus arquivos