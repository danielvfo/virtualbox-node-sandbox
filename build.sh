apt-get update <<-EOF
y
EOF

#----------Begin Docker setup----------
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common <<-EOF
y
EOF

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
apt-get update <<-EOF
y
EOF

apt-get install docker-ce docker-ce-cli containerd.io <<-EOF
y
EOF

cd /vagrant
docker build -t docked-node-server .
docker run -p 8080:8080 -d docked-node-server
#----------End Docker setup----------