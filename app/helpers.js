'use strict';

module.exports = class helpers {
  constructor() {};

  retrieveAllUsersWebsites(users) {
    return users.map(user => user.website);
  }

  retrieveNameEmailAndCompany(users) {
    let newUsers = users.map((user) => {
      return {name: user.name, email: user.email, company: user.company.name};
    });
    return this.sortUsersByName(newUsers);
  }

  retrieveUsersLivingInSuites(users) {
    return users.filter(user =>
      user.address.suite.toUpperCase().includes('SUITE') ||
      user.address.suite.toUpperCase().includes('SUÍTE'));
  }

  sortUsersByName(users) {
    users.sort(function(a, b) {
      var nameA = a.name.toUpperCase(); // ignore upper and lowercase
      var nameB = b.name.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // names must be equal
      return 0;
    });
    return users;
  }
};
